package com.tarnowski.am;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;

import jakarta.annotation.PostConstruct;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


/*
 * First endpoint Retrieves data: movie time and date and forwards them to GetMovie
 */
@WebServlet("/GetDateAndTime")
public class GetDateAndTime extends HttpServlet{

		private static final long serialVersion =1L;
		
		
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
			
			// Adding Polish characters and displaying messages as HTML code
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			
			//Downloading data to check the possibility of booking 
			//Data saved in date format to perform operations on them 
			//and in String for easy display
			String date = request.getParameter("date");
			String time = request.getParameter("time");
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			String DataTime = date + " " + time;
			Reservation.timeOfReservation = LocalDateTime.parse(DataTime, formatter);
			
			//First checks the date then the time and returns the appropriate message
			if(LocalDateTime.now().isBefore(Reservation.timeOfReservation.minusMinutes(15))) {
					
					//Creates pre-prepared movies and presents their list
					if(CreateMovies.allMoviesList.isEmpty()) {
						CreateScreeningRoom.createScreeningRooms();
						CreateMovies.createMovies();}
					
					//Message displaying videos
					response.getWriter().println("Filmy dostępne w dniu: " + date + "<br>");
					response.getWriter().println("o godzienie: " + time + "<br><br>");
			
						//I download and save information about the movie that the person wants to go to
						for(Movie movie : CreateMovies.allMoviesList) {if((request.getParameter("date").contains(movie.date) )& 
							(request.getParameter("time").contains(movie.time))) {response.getWriter().println(movie + "<br>");}}
						
						//Creates an html string that contains the endpoint to the next part of the form
						response.getWriter().println("<form action =\"GetMovie\" method=\"post\">"
								+ "<br><label for=\"date\">Wpisz tytuł dostępnego filmu na który chec kupić bilet:</label><br>"
								+ "<input type=\"text\" id=\"date\" name=\"movietitle\" value=\"tytuł\"><br>"
								+ "<br><input type = \"submit\" value=\"Prześlij\"><br></form>");
			
								// Ability to return to the home page again (undo selections)
								response.getWriter().println("<br><a href=\"http://localhost:8080/Cinema/\">Strona Główna</a>");
			
			} else {
				response.getWriter().println("nie można rezerwować seansów później niż 15 min przed rozpczeciem");
				
				//Ability to return to the home page again (undo selections)
				response.getWriter().println("<br><a href=\"http://localhost:8080/Cinema/\">Strona Główna</a>");
			}
		}
		
		protected void doPost(HttpServletRequest requset, HttpServletResponse response) throws IOException{
			
			doGet(requset, response);
		}
}
