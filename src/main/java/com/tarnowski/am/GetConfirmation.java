package com.tarnowski.am;

import java.io.IOException;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/*
 * displays booking information
 */

@WebServlet("/GetConfirmation")
public class GetConfirmation extends HttpServlet {
	
	private static final long serialVersion =1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		//Adding Polish characters and displaying messages as HTML code
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		//Saving the values sent from the GetMovie form
		String surname = request.getParameter("surname");
		String seats = request.getParameter("seats");
		String child= request.getParameter("ulgowe");
		String student = request.getParameter("studenckie");
		String adult = request.getParameter("normalne");
		
		//A series of if statements to check if the booking details were correct
		//We check whether the number of selected chairs and tickets is the same
		if(Reservation.checkNuberOfTicektIsCrrect(adult, student, child, seats)) {
		response.getWriter().println("Rezerwacja na nazwisko: " +"&nbsp" + Reservation.makeSurnameCapitalize(surname) + "&nbsp <br>");
		String systemAnserw = Reservation.doReservation(seats);
		response.getWriter().println(systemAnserw);
		
			//If the chair can be booked, the price and expiration date of the reservation will be calculated
			if(!systemAnserw.contains("nie można zerezerwować wybranych miejsc")) {
				response.getWriter().println("Do zapłaty: &nbsp" + Reservation.calculateTotalPrice(adult, student, child) + "<br>");
				response.getWriter().println("Za bilet należy zapłacić do : &nbsp" + Reservation.timeOfReservation.minusMinutes(15));
			}

		//Possibility to return to the start page again (undo selections)
		response.getWriter().println("<br><a href=\"http://localhost:8080/Cinema/\">Strona Główna</a>");
		
		} else {response.getWriter().println("Wybrana liczba krzeseł i biletów się nie zgadza");}
	}
	
	
	protected void doPost(HttpServletRequest requset, HttpServletResponse response) throws IOException{
		
		doGet(requset, response);
	}
}
