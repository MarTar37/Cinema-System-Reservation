package com.tarnowski.am;

import java.util.HashSet;

/*
 * The class that creates the movie object
 * It stores information about the screening room where the screening will take place
 * So that films played in the same hall do not duplicate their bookings
 * each instance of the Movie class contains a field with a screening room defined for it
 */
public class Movie {
	
	
	public ScreeningRoom screenigRoom;

	public final String title, date, time;
	public ScreeningRoom screeningRoom;
	
	public Movie(String title, String date, String time, ScreeningRoom screeningRoom) {
		this.title = title;
		this.time = time;
		this.date = date;
		// class field screenningRoom is created based on previously created room
		ScreeningRoom room = new ScreeningRoom(screeningRoom.screeningRoomName, screeningRoom.numberOfRows, screeningRoom.seatsInRow);
		this.screenigRoom = room;
	}
	
	@Override
	public String toString() {
		return title;
	}
}
