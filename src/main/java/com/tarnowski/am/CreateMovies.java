package com.tarnowski.am;

import java.util.HashSet;

/*
 * This class is responsible for creating new Movies
 * Assign them movies, time and date
 * It also keeps a list of all videos so that other methods can download them easily
 * For correct operation, the CreateScreeningRoom class must first create instances
 * ScreennigRoom objects
 */
public class CreateMovies {

	public static HashSet<Movie> allMoviesList = new HashSet<>();
	
	public static void createMovies() {
		
		CreateScreeningRoom.createScreeningRooms();  //Creates previously prepared Screening rooms
		
		Movie movie1 = new Movie("Fabelmanowie", "2023-02-01", "19:00", CreateScreeningRoom.allScreeningRooms.get(0));
		Movie movie2 = new Movie("Władca Pierścieni", "2023-02-01", "19:00", CreateScreeningRoom.allScreeningRooms.get(1));
		Movie movie3 = new Movie("Gwiezdne Wojny", "2023-02-01", "19:00", CreateScreeningRoom.allScreeningRooms.get(2));

		Movie movie4 = new Movie("Alien", "2023-02-01", "20:00", CreateScreeningRoom.allScreeningRooms.get(0));
		Movie movie5 = new Movie("Batman", "2023-02-01", "20:00", CreateScreeningRoom.allScreeningRooms.get(1));
		Movie movie6 = new Movie("Avatar", "2023-02-01", "20:00", CreateScreeningRoom.allScreeningRooms.get(2));
		
		//Add the created movies to the set 
		allMoviesList.add(movie1);
		allMoviesList.add(movie2);
		allMoviesList.add(movie3);
		allMoviesList.add(movie4);
		allMoviesList.add(movie5);
		allMoviesList.add(movie6);
	}

}
