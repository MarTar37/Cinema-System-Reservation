package com.tarnowski.am;

import java.io.IOException;
import java.util.HashSet;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


/*
 * Endpoint downloading data and forwarding information to the next form ->GetConfiramtion
 */
@WebServlet("/GetMovie")
public class GetMovie extends HttpServlet {

	private static final long serialVersion =1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		// Adding Polish characters and displaying messages as HTML code
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		//Displays the downloaded movie title
		response.getWriter().println("wybrany film: " + request.getParameter("movietitle"));
		CreateScreeningRoom.createScreeningRooms();
		
		// Loop checking which movie was selected and storing the selection in the Reservation class
		for(Movie movie : CreateMovies.allMoviesList) {
			if(request.getParameter("movietitle".trim()).contains(movie.title.trim())) {
				Reservation.selectedMovie = movie; 
			}
		}
		response.getWriter().println("<br>"+ Reservation.selectedMovie.screenigRoom.showScreenningRoom());
		

		//We will pass the information to the next endpoint using the HTML code
		//Transferring information about selected places
		response.getWriter().println("<form action =\"GetConfirmation\" method=\"post\">"
				+ "<br><label for=\"seats\">Wpisz numery miejsc które chcesz zarezerwować: </label><br>"
				+ "<input type=\"text\" id=\"date\" name=\"seats\" value=\"numery miejsc\"><br>");
		
		//Provision of personal data
		response.getWriter().println("<form action =\"GetConfirmation\" method=\"post\">"
				+ "<br><label for=\"name\">Podaj imie i nazwisko do rezerwacji : </label><br>"
				+ "<input type=\"text\" id=\"date\" name=\"name\" value=\"imie\"><br>"
				+ "<input type=\"text\" id=\"date\" name=\"surname\" value=\"nazwisko\"><br>");

		
		// Selection of ticket type
		response.getWriter().println("<form action =\"GetConfirmation\" method=\"post\">"
				+ "<br><label for=\"ticket\">podaj bliety jakie chcesz zarezerowować: </label><br>"
				+"<br><label for=\"ticket\">Ulgowy &nbsp  </label>"
				+ "<input type=\"text\" id=\"date\" name=\"ulgowe\" value=\"0 \"><br>"
				
				+"<br><label for=\"ticket\">Normalny &nbsp  </label>"
				+ "<input type=\"text\" id=\"date\" name=\"normalne\" value=\"0 \"><br>"
				
				+"<br><label for=\"ticket\">Studencki &nbsp  </label>"
				+ "<input type=\"text\" id=\"date\" name=\"studenckie\" value=\"0 \"><br>"
				
				+ "<br><input type = \"submit\" value=\"Prześlij\"><br></form>");
		
		//Ability to return to the home page again (undo selections)
		response.getWriter().println("<br><a href=\"http://localhost:8080/Cinema/\">Strona Główna</a>");
		
	}
	
	protected void doPost(HttpServletRequest requset, HttpServletResponse response) throws IOException{
		
		doGet(requset, response);
		}
	

}