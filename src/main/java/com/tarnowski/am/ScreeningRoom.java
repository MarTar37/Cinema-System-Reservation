package com.tarnowski.am;

import java.util.ArrayList;
import java.util.HashSet;

/*
 * The class allows you to create screening room
 * ScreeningRoom instances themselves will only be created in the CreateScreenningRoom class
 */
class ScreeningRoom {
	
	public String screeningRoomName;
	public int numberOfSeats;
	public int numberOfRows, seatsInRow;  //fields needed to create "graphics" representing the screening room
	public ArrayList<String> seats = new ArrayList<>();

	
	public ScreeningRoom(String screeningRoomName, int numberOfRows, int seatsInRow) {
		this.screeningRoomName = screeningRoomName;
		this.numberOfSeats = numberOfRows*seatsInRow;
		this.numberOfRows = numberOfRows;
		this.seatsInRow = seatsInRow;
		for(int c =1; c<=this.numberOfSeats; c++) {seats.add(Integer.toString(c));}

	}
	
	//method creating a graphic image of the screening room for the end user
	public String showScreenningRoom() {
		String avaibleSeats = "<br>____||__EKRAN__||____ <br><br>";  //Screen graphics will have rows in front of it
		for(int c = 0; c <= numberOfSeats-1; c++) {
			String seat = seats.get(c);
			if(c%seatsInRow != 0) {
				avaibleSeats += seat + "&nbsp";
			} else {avaibleSeats += "<br>" + seat + "  ";}
		}
		return avaibleSeats;
	}
	
	@Override
	public String toString() {
		return "sala ma"+numberOfSeats;
	}
}
