package com.tarnowski.am;

import java.util.ArrayList;

/*
 * The class creates instances of screening room
 * And saves it in Array, it allows you to easily select the room by index
 */

public class CreateScreeningRoom {

	public static ArrayList<ScreeningRoom> allScreeningRooms = new ArrayList<>();
	
	public static void createScreeningRooms() {
		
		ScreeningRoom room1 = new ScreeningRoom("Sala imienia Krzysztofa Kieślowskiego", 10, 10);
		ScreeningRoom room2 = new ScreeningRoom("Sala imienia Andrzeja Wajdy", 8, 12);
		ScreeningRoom room3 = new ScreeningRoom("Sala imienia Stanisława Bareji", 11, 9);

		CreateScreeningRoom.allScreeningRooms.add(room1);
		CreateScreeningRoom.allScreeningRooms.add(room2);
		CreateScreeningRoom.allScreeningRooms.add(room3);
		
	}
}
