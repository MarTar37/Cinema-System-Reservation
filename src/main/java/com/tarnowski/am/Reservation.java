package com.tarnowski.am;

import java.time.LocalDate;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;

/*
 * Class containing methods and fields needed to manage our reservation
 */
public class Reservation {
	
	//A field remembering the session selected by the user
	public static Movie selectedMovie;
	
	//Ticket prices and types
	private static final double childTicket = 12.50;
	private static final double adultTicket = 25.00;
	private static final double studentTicket = 18.00;
	
	//Stored session time (in date format)
	public static LocalDateTime timeOfReservation; 

	//A method that calculates from the ticket price based on the data received by the user
	public static String calculateTotalPrice(String numberOfAdultTickets, String numberOfStudentsTicket, String numberOfChildTicket) {
		
		int adult = Integer.parseInt(numberOfAdultTickets.trim());
		int student = Integer.parseInt(numberOfStudentsTicket.trim());
		int child = Integer.parseInt(numberOfChildTicket.trim());
		
		String totalPrice = String.valueOf((adult*adultTicket) + (child*childTicket) + (student*studentTicket));
		
		return totalPrice;
	}
	
	/*
	* The method creates reservations after passing two tests defined in the Reservation class
	* tests: checkIfReservationIsCorrect & checkIfSeatIsAvaible
	* The first checks whether there are no single places between reservations
	* Second if the seat is not already occupied
	* Occupied places are replaced with X &nbsp so that the room displayed to the user retains its shape
	 */
	public static String doReservation(String seatsString){
		
		String result = "";
		String [] nubersOfReservationSeats = seatsString.split(" ");
		
		boolean isEmpty = checkIfSeatIsAvaible(nubersOfReservationSeats);
		boolean isCorrect = checkIfReservationIsCorrect(nubersOfReservationSeats);
		try {
		if((isEmpty && isCorrect) == true) {
			for(String seat : nubersOfReservationSeats) {
				int index = Integer.parseInt(seat);
				selectedMovie.screenigRoom.seats.set(index-1, "X &nbsp");
				result = "Miejsca: &nbsp" + seatsString + " &nbsp zostały pomyślnie zarezerwowane <br>";
			}
		} else {result = "nie można zerezerwować wybranych miejsc" ;}
		} catch(IndexOutOfBoundsException e) {}
		
		return result;
	}
	
	//Checks if the slot is occupied
	//The occupied space is marked as X &nbsp
	private static boolean checkIfSeatIsAvaible(String[] nubersOfReservationSeats) {
		boolean isEmpty = true;

			for(String seat : nubersOfReservationSeats) {
				int index = Integer.parseInt(seat);
				if(selectedMovie.screenigRoom.seats.get(index-1).contains("X &nbsp")) {isEmpty = false;}
			}
		return isEmpty;
	}
	
	//Checks if there are no gaps between reservations
	//Looks for two places to the right of the selected one and two places to the left
	private static boolean checkIfReservationIsCorrect(String[] nubersOfReservationSeats){
		boolean isCorrect = true;
		boolean leftSide1 = true;
		boolean leftSide2 = true;
		boolean rightSide1 = true;
		boolean rightSide2 = true;

			for(String seat : nubersOfReservationSeats) {
				int index = Integer.parseInt(seat);
				try {
					leftSide1 = selectedMovie.screenigRoom.seats.get(index-3).contains("X &nbsp");
					leftSide2 = !selectedMovie.screenigRoom.seats.get(index-2).contains("X &nbsp");
					rightSide1 = !selectedMovie.screenigRoom.seats.get(index).contains("X &nbsp");
					rightSide2 = selectedMovie.screenigRoom.seats.get(index+1).contains("X &nbsp");
						
					if((leftSide1 && leftSide2) || (rightSide1 && rightSide2)) {isCorrect = false;}
				} catch(IndexOutOfBoundsException e) {}

			}	
		return isCorrect;
	}
	
	//Ensures that even names without capital letters will be spelled correctly
	//Only works for names with or without a single "-".
	public static String makeSurnameCapitalize(String surname) {
		String capitalizeSurname = "";
		if(surname.contains("-")) {
			String[] surnameList = surname.split("-");
			capitalizeSurname += makeFirstLetterCapitalize(surnameList[0])+"-"+makeFirstLetterCapitalize(surnameList[1]);

		} else {
			capitalizeSurname += makeFirstLetterCapitalize(surname);
		}
		return capitalizeSurname;
	} 
	
	//method used in the makeSurnameMethodCapitalize 
	//Changes the first letter of the String to uppercase
	private static String makeFirstLetterCapitalize(String name) {
		String newName = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
		return newName;
	}
	
	//The method checks if the number of tickets and selected seats is the same
	public static boolean checkNuberOfTicektIsCrrect(String numberOfAdultTickets, String numberOfStudentsTicket,
			String numberOfChildTicket, String seatsString) {
		
		boolean respone = false;
		int adult = Integer.parseInt(numberOfAdultTickets.trim());
		int student = Integer.parseInt(numberOfStudentsTicket.trim());
		int child = Integer.parseInt(numberOfChildTicket.trim());
		
		String[] seatsNumber = seatsString.split(" ");
		if(seatsNumber.length == adult + student + child) {respone = true;}
		return respone;
	}
}
	


